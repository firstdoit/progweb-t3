<jsp:include page="/WEB-INF/main-header.jsp"></jsp:include>
Administração

<form method="POST" action="AdminServlet">
	<input type="submit" value="Gerar lista de votantes">
</form>

<form method="GET" action="AdminServlet">
	<input type="submit" value="Ver resultados">
</form>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${requestScope.list1 != null && requestScope.list1.size() > 0}">
<p>Grupo 1 - Proprietários:</p> 
	<ul>
		<c:forEach items="${requestScope.list1}" var="person" varStatus="loop">
			<li <c:if test="${loop.index < 6}">style="background-color:#BBFFBB"</c:if>>
				<span>Nome:</span>
				<span> ${person.username} </span>
				<span>Votos:</span>
				<span> ${person.votesReceived} </span>
			</li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${requestScope.list2 != null && requestScope.list2.size() > 0}">
<p>Grupo 2 - Locatários:</p> 
	<ul>
		<c:forEach items="${requestScope.list2}" var="person" varStatus="loop">
			<li <c:if test="${loop.index < 6}">style="background-color:#BBFFBB"</c:if>>
				<span>Nome:</span>
				<span> ${person.username} </span>
				<span>Votos:</span>
				<span> ${person.votesReceived} </span>
			</li>
		</c:forEach>
	</ul>
</c:if>

<c:if test="${requestScope.list3 != null && requestScope.list3.size() > 0}">
<p>Grupo 3 - Funcionários:</p> 
	<ul>
		<c:forEach items="${requestScope.list3}" var="person" varStatus="loop">
			<li <c:if test="${loop.index < 1}">style="background-color:#BBFFBB"</c:if>>
				<span>Nome:</span>
				<span> ${person.username} </span>
				<span>Votos:</span>
				<span> ${person.votesReceived} </span>
			</li>
		</c:forEach>
	</ul>
</c:if>
<jsp:include page="/WEB-INF/main-footer.jsp"></jsp:include>
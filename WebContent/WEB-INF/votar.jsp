<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/main-header.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
	<c:if test="${sessionScope.username != null}">
		<span>Usuário logado:<c:out value="${sessionScope.username}" /></span>
	</c:if>
</div>
<c:if test="${requestScope.voted == null || requestScope.voted == false}">
	Vote em <c:out value="${requestScope.numberOfVotes}"></c:out> candidatos.
	Qualquer outra quantidade de votos implicará em anulação do voto!
	<form method="POST" action="Votar">
	<ul>
		<c:forEach items="${requestScope.list}" var="person">
			<li>
				<input type="checkbox" value="${person.id}" name="person"/>
				<span> ${person.username} </span>
			</li>
		</c:forEach>
	</ul>
	<input type="submit" value="Votar"/>
	</form>
</c:if>
<jsp:include page="/WEB-INF/main-footer.jsp"></jsp:include>
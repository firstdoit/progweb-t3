<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Votação</title>
<!-- CSS -->
<link rel="stylesheet" media="screen" href="<%=request.getContextPath()%>/bootstrap/css/bootstrap.css">
<link rel="stylesheet" media="screen" href="<%=request.getContextPath()%>/css/main.css">
<!-- /CSS -->

</head>
<body>
<div class="container">
<h2>Balança Mas Cai</h2>
<p>
<%= request.getAttribute("message") != null ? request.getAttribute("message") : ""%>
</p>
package model;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import util.HibernateUtil;

@Entity
public class Person {
	public static final int GRUPO_PROPRIETARIOS = 1;
	public static final int NUM_PROPRIETARIOS = 6;
	public static final int GRUPO_LOCATARIOS = 2;
	public static final int NUM_LOCATARIOS = 6;
	public static final int GRUPO_FUNCIONARIOS = 3;
	public static final int NUM_FUNCIONARIOS = 1;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Integer votesReceived = 0;
	private Boolean hasVoted = false;
	/**
	 * Um de [GRUPO_PROPRIETARIOS, GRUPO_LOCATARIOS, GRUPO_FUNCIONARIOS]
	 */
	private Integer groupNumber = 0;
	private Boolean inCommission = false;
	@Column(columnDefinition="varchar(255) CHARACTER SET utf8 COLLATE utf8_bin", nullable=false)
	private String username;
	private String password;
	private Date birthdate;

	public Person() {
		super();
	}

	public Person(Integer group, Boolean inCommission, String name,
			String password, Date birthdate) {
		super();
		this.groupNumber = group;
		this.inCommission = inCommission;
		this.username = name;
		this.password = password;
		this.birthdate = birthdate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getVotesReceived() {
		return votesReceived;
	}

	public void setVotesReceived(Integer votesReceived) {
		this.votesReceived = votesReceived;
	}

	public Boolean getHasVoted() {
		return hasVoted;
	}

	public void setHasVoted(Boolean hasVoted) {
		this.hasVoted = hasVoted;
	}

	public Boolean getInCommission() {
		return inCommission;
	}

	public void setInCommission(Boolean inCommission) {
		this.inCommission = inCommission;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Integer getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(Integer groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Registra o voto dessa pessoa na lista de pessoas persons
	 * 
	 * @return true se voto válido, false se voto anulado.
	 * @throws InvalidVoteException
	 *             caso as pessoas não sejam todas do grupo desta pessoa.
	 */
	public boolean vote(List<Person> persons) throws InvalidVoteException {
		// Checa se todas as pessoas da lista são do mesmo grupo do votante.
		if (persons != null)
			for (Person p : persons) {
				if (p.getGroupNumber() != this.getGroupNumber())
					throw new InvalidVoteException();
			}

		// Independente se o voto foi válido ou anulado, ele está feito.
		this.hasVoted = true;
		this.save();

		// Checa se o número de pessoas votadas consiste com o grupo do votante.
		if (persons.size() != getRequiredNumberOfVotes())
			return false;

		for (Person p : persons) {
			// Aumenta o número de votos dessa pessoa.
			p.votesReceived++;
			// Salva a pessoa votada.
			p.save();
		}

		// Sucesso
		return true;
	}

	public Person save() {
		Session session;
		SessionFactory sessionFactory;
		Transaction transacao;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			transacao = session.beginTransaction();
			System.out.println("Salvando " + this);
			session.saveOrUpdate(this);
			transacao.commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return this;
	}

	public static Person getPersonById(Long id) {
		SessionFactory sessionFactory;
		Session session;
		String queryString = "from Person person where person.id=" + id;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			Query query = session.createQuery(queryString);
			Person person = (Person) query.uniqueResult();
			System.out.println("Recuperei usuário por id: " + person);
			session.close();
			return person;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Person getPersonByNameAndPassword(String name, String password) {
		SessionFactory sessionFactory;
		Session session;
		//Sanitizing input do usuário.
		String queryString = "from Person person where person.username='" + name + "' " +
				"and person.password='" + password + "'";
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			Query query = session.createQuery(queryString);
			Person person = (Person) query.uniqueResult();
			System.out.println("Recuperei usuário por nome: " + person);
			session.close();
			return person;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<Person> getPersonsByGroupAndMostVoted(int group) {
		SessionFactory sessionFactory;
		Session session;
		String queryString = "from Person person where person.groupNumber ="+group+" " +
				"order by person.votesReceived desc, person.birthdate asc";
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			Query query = session.createQuery(queryString);
			List<Person> list = query.list();
			session.close();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<Person> getVotablePersonsByGroup(int group) {
		SessionFactory sessionFactory;
		Session session;
		String queryString = "from Person p where p.groupNumber=" + group + " and p.inCommission=false order by p.username";
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			System.out.println("Executando: " + queryString);
			Query query = session.createQuery(queryString);
			List<Person> list = (List<Person>) query.list();
			session.close();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void generatePersonList(List<Person> persons) {
		SessionFactory sessionFactory;
		Session session;
		String queryString = "delete from Person";
		Transaction transacao;
		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			transacao = session.beginTransaction();
			Query query = session.createQuery(queryString);
			query.executeUpdate();
			transacao.commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (Person p : persons) {
			p.save();
		}
	}
	
	public int getRequiredNumberOfVotes() {
		if (GRUPO_FUNCIONARIOS == this.getGroupNumber())
			return NUM_FUNCIONARIOS*2;
		else if (GRUPO_LOCATARIOS == this.getGroupNumber())
			return NUM_LOCATARIOS*2;
		else if (GRUPO_PROPRIETARIOS == this.getGroupNumber())
			return NUM_PROPRIETARIOS*2;
		
		return 0;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", votesReceived=" + votesReceived
				+ ", hasVoted=" + hasVoted + ", groupnumber=" + groupNumber
				+ ", inCommision=" + inCommission + ", username=" + username
				+ ", password=" + password + ", birthdate=" + birthdate + "]";
	}
}

package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Person;

public class SecurityUtils {
	public final static String AUTHENTICATE_FLAG = "authenticated";
	public final static String USER_ID = "userid";
	public final static String USER_NAME = "username";
	
	public static boolean authenticate(String name, String password, HttpSession session) {
		System.out.println("Checando user/password: " + name + " " + password);
		Person person = Person.getPersonByNameAndPassword(name, password);
		System.out.println("User encontrado: " + person);
		if (person != null) {
			session.setAttribute(AUTHENTICATE_FLAG, "true");
			session.setAttribute(USER_ID, person.getId());
			session.setAttribute(USER_NAME, person.getUsername());
			System.out.println("Guardei id: " + person.getId());
			return true;
		}
		return false;
	}
	
	public static void logout(HttpSession session) {
		session.removeAttribute(AUTHENTICATE_FLAG);
		session.removeAttribute(USER_ID);
		session.removeAttribute(USER_NAME);
	}
	
	public static boolean isAuthenticated(HttpSession session) {
		return "true".equals(session.getAttribute(AUTHENTICATE_FLAG));
	}
	
	public static boolean checkAuthentication(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!SecurityUtils.isAuthenticated(request.getSession())) {
			request.getRequestDispatcher("/login.jsp").forward(request, response);
			return false;
		}
		return true;
	}

	public static long getUserID(HttpSession session) {
		Long id = (Long) session.getAttribute(USER_ID);
		System.out.println("Recuperei id logado: " + id);
		return id;
	}
	
}

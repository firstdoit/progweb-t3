package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.InvalidVoteException;
import model.Person;

/**
 * Servlet implementation class Vote
 */
@WebServlet("/Votar")
public class Vote extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vote() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Mostra a lista de pessoas em quem esse usuário pode votar
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!SecurityUtils.checkAuthentication(request, response)) 
			return;

		Person loggedUser = Person.getPersonById(SecurityUtils.getUserID(request.getSession()));
		System.out.println("Usuario tentando votar: " + loggedUser);
		if (loggedUser.getHasVoted()) {
			request.setAttribute("voted", true);
			request.setAttribute("message", "Você já votou!");
		}
		else {
			int group = loggedUser.getGroupNumber();
			List<Person> list = Person.getVotablePersonsByGroup(group);
			if (list == null) {
				request.setAttribute("message", "Houve um erro ao recuperar as pessoas. Fale com o administrador.");
			}
			else {
				request.setAttribute("numberOfVotes", loggedUser.getRequiredNumberOfVotes());
				request.setAttribute("list", list);
			}
		}
		
		request.getRequestDispatcher("WEB-INF/votar.jsp").forward(request, response);
	}

	/**
	 * Faz o voto desse usuário na lista de pessoas
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!SecurityUtils.checkAuthentication(request, response)) 
			return;

		Person loggedUser = Person.getPersonById(SecurityUtils.getUserID(request.getSession()));
		if (loggedUser.getHasVoted()) {
			request.setAttribute("voted", true);
			request.setAttribute("message", "Você já votou!");
			request.getRequestDispatcher("WEB-INF/votar.jsp").forward(request, response);
			return;
		}
		
		String[] idArray = request.getParameterValues("person");
		List<Person> list = new ArrayList<Person>();
		try {
			for (String id : idArray) {
				list.add(Person.getPersonById(Long.parseLong(id)));
			}
		} catch (NumberFormatException e) {
			request.setAttribute("message", "Houve um erro ao recuperar as pessoas. Fale com o administrador.");
			request.getRequestDispatcher("WEB-INF/votar.jsp").forward(request, response);
			return;
		}
		
		try {
			//Voto válido
			if (loggedUser.vote(list))
			{
				request.setAttribute("voted", true);
				request.setAttribute("message", "Seu voto foi registrado com sucesso.");
			}
			//Voto anulado
			else {
				request.setAttribute("voted", true);
				request.setAttribute("message", "Seu voto foi anulado.");
			}
		} catch (InvalidVoteException e) {
			e.printStackTrace();
			request.setAttribute("message", "Houve um problema ao processar seu voto. Tente novamente.");
		}
		request.getRequestDispatcher("WEB-INF/votar.jsp").forward(request, response);
	}

}

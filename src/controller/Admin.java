package controller;

import java.io.IOException;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Person;
import util.FileUtils;

/**
 * Servlet implementation class Vote
 */
@WebServlet("/AdminServlet")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin() {
        super();
    }

	/**
	 * Ver resultados da eleição
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	List<Person> list1 = Person.getPersonsByGroupAndMostVoted(Person.GRUPO_PROPRIETARIOS);
    	List<Person> list2 = Person.getPersonsByGroupAndMostVoted(Person.GRUPO_LOCATARIOS);
    	List<Person> list3 = Person.getPersonsByGroupAndMostVoted(Person.GRUPO_FUNCIONARIOS);
		if (list1 == null || list2 == null || list3 == null) {
			request.setAttribute("message", "Houve um erro ao recuperar as pessoas. Fale com o administrador.");
		}
		else {
			request.setAttribute("list1", list1);
			request.setAttribute("list2", list2);
			request.setAttribute("list3", list3);
		}
		request.getRequestDispatcher("WEB-INF/adm.jsp").forward(request, response);
	}

	/**
	 * Gerar lista de candidatos
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String condominio = FileUtils.readFile("/WEB-INF/Condominio.csv", getServletContext());
    	String[] moradores = condominio.split("\n");
    	List<Person> persons = new ArrayList<Person>();
    	for (String s : moradores) {
    		String[] atributos = s.split(",");
    		Calendar cal = GregorianCalendar.getInstance();
    		cal.set(Integer.parseInt(atributos[1]), Integer.parseInt(atributos[2])-1, Integer.parseInt(atributos[3]),0,0,0);
    		Date date = cal.getTime();
    		Person person = new Person(Integer.parseInt(atributos[5]), Boolean.parseBoolean(atributos[6]), atributos[0], atributos[4], 
    				date);
    		persons.add(person);
    		System.out.println(person);
    	}
    	Person.generatePersonList(persons);
		request.setAttribute("message", "Lista gerada com sucesso.");
		request.getRequestDispatcher("WEB-INF/adm.jsp").forward(request, response);
	}

}

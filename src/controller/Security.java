package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SecurityServlet
 */
@WebServlet(urlPatterns={"/Security", "/logout", "/Security/logout"})
public class Security extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Security() {
        super();
    }

	/**
	 * Logout
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getRequestURI().contains("logout")) {
			SecurityUtils.logout(request.getSession());
			response.sendRedirect("");
			return;
		}
		response.sendRedirect("login.jsp");
	}

	/**
	 * Login
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = (String) request.getParameter("name");
		String pass = (String) request.getParameter("password");
		if (!SecurityUtils.authenticate(name, pass, request.getSession())) {
			System.out.println("Password nao confere: " + pass);
			response.setStatus(403);
			request.setAttribute("message", "A combinação usuário/senha digitada está incorreta. Tente novamente.");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		System.out.println("Password confere.");
		response.sendRedirect("Votar");
	}

}

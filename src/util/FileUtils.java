package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.servlet.ServletContext;

public class FileUtils {
	
	public static String readFile( String file, ServletContext context ) throws IOException {
		String contextPath = context.getRealPath("/");
	    BufferedReader reader = new BufferedReader( new FileReader(contextPath + file));
	    String line  = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    String ls = System.getProperty("line.separator");
	    while( ( line = reader.readLine() ) != null ) {
	        stringBuilder.append( line );
	        stringBuilder.append( ls );
	    }
	    return stringBuilder.toString();
	 }
}
